#!/usr/bin/env python3
#-*- coding: utf-8 -*-

from matrix import Mat

def mat_mul_vec(mat, vec):
    """Return mat * vec where mat is a matrix and vec a vector."""

    assert (mat.dim[0] == mat.dim[1] == vec.dim)

    x = []
    for k in range(vec.dim):
        x.append(Vec(mat[k]) * vec)

    return Vec(x)

class Vec:

    def __init__(self, value):
        """..."""

        self.value = tuple(value)
        self.dim = len(value)

    def __repr__(self):
        """Show as a string the Vector."""

        return str(self.value)

    def __getitem__(self, index):
        """Return self[index]."""
        
        return self.value[index]

    def __add__(self, value):
        """Return self + value."""

        assert (type(value) == Vec) and (self.dim == value.dim)
        
        return Vec([self[k] + value[k] for k in range(self.dim)])

    def __sub__(self, value):
        """Return self - value."""

        assert (type(value) == Vec) and (self.dim == value.dim)
        
        return Vec([self[k] - value[k] for k in range(self.dim)])

    def __rmul__(self, value):
        """Return value * self ."""

        if (type(value) in [int, float, complex]):
            return Vec([value * self[k] for k in range(self.dim)])
        elif (type(value) == Mat):
            return mat_mul_vec(value, self)
        #else:
        return value.__mul__(value)
        

    def __mul__(self, value):
        """Return the dot product of self and value."""

        assert (type(value) == Vec) and (self.dim == value.dim)

        dot = 0
        for k in range(self.dim):
            dot = dot + self[k].conjugate() * value[k]

        return dot

    def __pow__(self, value):
        """If self and value are 3D-Vectors : Return the cross product."""

        assert (type(value) == Vec) and (self.dim == 3 == value.dim)

        x = self[1] * value[2] - self[2] * value[1]
        y = self[2] * value[0] - self[0] * value[2]
        z = self[0] * value[1] - self[1] * value[0]

        return Vec((x, y, z))

    def norm(self, p=2):
        """Return the p-norm of self, with p >= 1 or p == float("inf").
By default, return the euclidean norm of self."""

        if (p == 2):
            return (self*self) ** (1/2)
        elif (p >= 1):
            return sum( [abs(self[k]) ** p for k in range(self.dim)] ) ** (1/p)
        elif (p == float("inf")):
            return max( [abs(self[k]) for k in range(self.dim)] )
        #else:
        raise Exception("Norm p is not defined for p={}.".format(p))
        
